from __future__ import print_function, division
from builtins import range

import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import beta
import pandas as pd

df = pd.read_csv('popads_pl_data.csv')
a = df['is_sale']

print("a.mean:", a.mean())

class Model(object):
  def __init__(self):
    self.a = 1
    self.b = 1

  def sample(self):
    return np.random.beta(self.a, self.b)

  def update(self, x):
    self.a += x
    self.b += 1 - x
    
def plot(model, z):
  x = np.linspace(beta.ppf(0, model.a, model.b) * 100, beta.ppf(0.999, model.a, model.b) * 100, 200)
  y = beta.pdf(x, model.a, model.b, scale = 100)
  print("95 percents confidence level x = %.7f" % beta.ppf(0.975, model.a, model.b))
  print("Median is x = %.5f" % beta.median(model.a, model.b, scale=100))
  print("Interval is x = {0}".format(beta.interval(0.95, model.a, model.b, scale=100)))
  plt.plot(x, y, label="CV distribution for %s" % z)
  plt.legend()
  plt.show() 
 
m = Model()
t = 0
z = 0
for i in a:
  z += 1
  m.update(i)
  if i == 1:
    t += 1
  #if z % 500 == 0:
print("Impressions: %d" % z)
print("Number of conversions: %d" % t)
plot(m, z)
