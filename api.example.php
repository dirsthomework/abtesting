<?php

require_once __DIR__ . "/vendor/autoload.php";

use AffMarketingScripts\TrafficSourcePlugins\PlugrushPlugin;
use AffMarketingScripts\TrafficSourcePlugins\PopadsPlugin;
use AffMarketingScripts\TrafficSourcePlugins\PopcashPlugin;
use AffMarketingScripts\TrackerManagers\KeitaroManager;
use AffMarketingScripts\AbBayesianBandit;
use TelegramBot\Api\BotApi;

// To handle HTML dom files.
define('MAX_FILE_SIZE', 1000000);

/** PLUGRUSH **/
//
//$plugrush = new PlugrushPlugin(
//  'su.com',
//  '',
//  396487
//  );
//$x = $plugrush->getReportByZones(7809784);
//$plugrush->setBlacklistZones(396487, [320417, 329121]);

/** POPCASH **/

//$popcash = new PopcashPlugin($apiKey);
//$x = $popcash->getReportByZones(284813, 0, strtotime("-30 days", time()));
//$x = $popcash->setBlacklistZones(284813, [322323, 32323]);

/** POPADS **/

//$popads = new PopadsPlugin($apiKey = 'sss');
//$popads->login($login, $password);
//$popads->setBlacklistZones(6594551, [23232]);
//$x = $popads->getReportByZones(6594551);

/*******************/
/****  KEITARO  ****/
/*******************/
//$mngr = new KeitaroManager("keitaro", "*.*.*.*", 3306, "root","****");
//$mngr->enableLandingOnly(8, 8);
//$mngr->enableOfferOnly(8, 8);
//$x = $mngr->getWebsitesReport(6);
//$x = $mngr->getConversionsDataForCampaign(6, [], [], [6], [1], KeitaroManager::SALE);

/******************************************
 * CHECK CONF INTERVALS CALCULATIONS - PYTHON CALCS = PHP CALCS
/******************************************/
//$csv = array_map(
//  'str_getcsv',
//  file(__DIR__ . '/__beta_model_example/popads_pl_data.csv')
//);
//
//$x = array_shift($csv);
//
//$csv = array_map(function ($e) { return $e[0]; }, $csv);
//$csv = array_chunk($csv, 500);
//$b = new AbBayesianBandit($csv[0], 1);
//
//$x = $b->getConfidenceIntervalUpper(0.95);
//$x = sprintf("%.10f",$x);
//$x = $b->getConfidenceIntervalLower(0.95);
//$x = sprintf("%.10f",$x);
//$x = count($csv);

// PYTHON Interval is x = (0.041567058757010833, 0.066287891397309728) 95% confidence.
// PHP INTERVAL = 0.00041567058757064,0.00066287891397376
// Also checked on 500 impressions with no conversions - shows equal results
// Conclusion PHP calculations are equal to python ones.

//define("TELEGRAM_BOT_API", "0000");
//$messenger = new BotApi(TELEGRAM_BOT_API);
//$x = $messenger->sendMessage("", "EXCLUDED 000");

print_r($x);
print PHP_EOL;