<?php

require_once __DIR__ . "/vendor/autoload.php";

//use MathPHP\Probability\Distribution\Table;

//$table = Table\StandardNormal::Z_SCORES;
//$probability = $table["1.4"][8];

//print_r($table);
//print_r($probability);


echo "How many conversions? ";
$conversions = rtrim(fgets(STDIN));

if ($conversions == 1) {
  echo "C'mon Dude, Don't you wanna to test a bit more? \n";
  echo "==============================================\n";
}

echo "How many clicks/impressions? ";
$total = rtrim(fgets(STDIN));

echo "What is the payout ($)? ";
$payout = rtrim(fgets(STDIN));

echo "What is your click cost ($)? ";
$clickCost = rtrim(fgets(STDIN));

echo "What minimum ROI (%) you wanna stick with? ";
$minROI = rtrim(fgets(STDIN));

$minCTR = ($minROI/100+1) * $clickCost / $payout * 100;
print $minCTR;

$interval = new OzdemirBurak\Sancus\Intervals\WilsonInterval($conversions, $total - $conversions, 0.95);

echo "==============================================\n";
print "Lower possible CTR: " . 100*$interval->getLowerBound() . "%\n";
print "Upper possible CTR: " . 100*$interval->getUpperBound() . "%\n";
echo "==============================================\n";

if ($minCTR >= 100*$interval->getUpperBound()) {
  echo "It is not worth it";
} elseif ($minCTR <= 100*$interval->getLowerBound()) {
  echo "This is a winner. Somebody is gonna make big bucks"; 
} else {
  echo "Min CTR $minCTR% is in the Confidence range. It's worth to be tested a bit more";
}
echo "\n";

//print_r($interval->getInterval()); // Array([0] => 0.67282656258608, [1] => 0.81400207671582)
//echo $interval->getScore();
