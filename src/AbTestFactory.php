<?php

namespace AffMarketingScripts;

use AffMarketingScripts\TrackerManagers\TrackerManagerInterface;
use AffMarketingScripts\TrafficSourcePlugins\TrafficSourcePluginInterface;
use TelegramBot\Api\BotApi;

/**
 * Class AbTestFactory
 * Init abtest class object.
 *
 * @package AffMarketingScripts
 */
class AbTestFactory {

  /**
   * @var string
   */

  CONST ABTEST_CLASSES_FOLDER = __DIR__ . "/AbTests";

  /**
   * Get array of test types.
   *
   * @return array
   *   Keys is machine name and values are classes.
   */
  public static function getMappedAbTests() {

    $tests = array_filter(
      scandir(self::ABTEST_CLASSES_FOLDER),
      function($fname) {
        return !in_array($fname, ['.', '..']);
      }
    );

    $return = [];

    foreach ($tests as $test) {
      $test = basename($test, ".php");
      $currentClass = "\\AffMarketingScripts\\AbTests\\" . $test;
      $class = new \ReflectionClass($currentClass);
      if (
        $class->isSubclassOf('\\AffMarketingScripts\\AbTests\\AbTestBase')
      ) {
        $return[$test] = $currentClass;
      }
    }

    return $return;
  }

  /**
   * Init Ab Test object.
   *
   * @param \AffMarketingScripts\TrackerManagers\TrackerManagerInterface $tracker
   *   Tracker manager object.
   * @param \AffMarketingScripts\TrafficSourcePlugins\TrafficSourcePluginInterface $trafficSource
   *   Traffic source plugin object.
   * @param $abTestData
   *   Ab test config data.
   *
   * @return \AffMarketingScripts\AbTests\AbTestInterface
   *   AbTest object whcih implements interface.
   *
   * @throws \Exception
   *   On ab test not found.
   */
  public static function getAbTest(
    TrackerManagerInterface $tracker,
    TrafficSourcePluginInterface $trafficSource,
    BotApi $messenger,
    $abTestData
  ) {
    $maps = self::getMappedAbTests();

    if (isset($maps[$abTestData['name']])) {
      return new $maps[$abTestData['name']]($tracker, $trafficSource, $messenger, $abTestData);
    } else {
      throw new \Exception("Couldn't init abtest object. Wrong config name?");
    }

  }

}