<?php

namespace AffMarketingScripts\Commands;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Console\Helper\ProgressBar;

/**
 * Class AbTestActivateCommand
 *
 * @package AffMarketingScripts\Commands
 */
class AbTestActivateCommand extends AbTestCommandBase {

  /**
   * @const define status.
   */
  const DISABLE = "DISABLE";
  const ENABLE  = "ENABLE";
  
  /**
   * @var string
   */
  protected static $defaultName = 'ab-test:activate';

  /**
   * {@inheritdoc}
   */
  protected function configure()
  {
    $this
      ->setDescription('Activate traffic test')
      ->setHelp('Interactive command to define status for A/B test configured with init');
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $helper = $this->getHelper("question");

    // Type of the test.
    $question = new ChoiceQuestion(
      "What do we do?: ",
      [self::DISABLE, self::ENABLE],
      0
    );
    $question->setErrorMessage("Your choice is incorrect.");
    $status = $helper->ask($input, $output, $question);
    $statusBool = $status == self::ENABLE;

    $parsed = self::getAllParsedConfigs();

    // Get all configs with chosen status.
    $choices = self::getConfigNamesWithStatus(!$statusBool, $parsed);

    if (empty($choices)) {
      $output->writeln("There are no tests to $status yet.");
      return 0;
    }

    // Perform status change
    $question = new ChoiceQuestion(
      "What configs do you want to {$status} (Enter comma separated list)?: ",
      $choices
    );
    $question->setErrorMessage("Your choice is incorrect.");
    $question->setMultiselect(true);
    $picked = $helper->ask($input, $output, $question);

    $progressBar = new ProgressBar($output, count($picked));
    $progressBar->start();

    foreach ($picked as $file) {
      $parsed[$file]['status'] = (int) $statusBool;
      $yaml = Yaml::dump($parsed[$file]);
      file_put_contents(self::CONFIGS_FOLDER . "/" . $file, $yaml);
      $progressBar->advance();
    }

    $progressBar->finish();
    $output->writeln("");

    return 0;
  }

}