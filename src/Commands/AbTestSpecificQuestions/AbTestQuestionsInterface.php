<?php

namespace AffMarketingScripts\Commands\AbTestSpecificQuestions;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Question\Question;

/**
 * Interface AbTestQuestionsInterface
 *
 * @package AffMarketingScripts\Commands\AbTestSpecificQuestions
 */
interface AbTestQuestionsInterface {

  /**
   * @param \AffMarketingScripts\Commands\AbTestSpecificQuestions\InputInterface $input
   * @param \AffMarketingScripts\Commands\AbTestSpecificQuestions\OutputInterface $output
   * @param \AffMarketingScripts\Commands\AbTestSpecificQuestions\QuestionHelper $helper
   * @param string $class
   *
   * @return mixed
   */
  public static function ask(
    InputInterface $input,
    OutputInterface $output,
    QuestionHelper $helper,
    string $class
  );

}