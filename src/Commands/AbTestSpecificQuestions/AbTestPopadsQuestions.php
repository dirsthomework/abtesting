<?php

namespace AffMarketingScripts\Commands\AbTestSpecificQuestions;

use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AffMarketingScripts\Commands\AbTestCommandBase;

class AbTestPopadsQuestions implements AbTestQuestionsInterface {

  /**
   * {@inheritdoc}
   */
  public static function ask(
    InputInterface $input,
    OutputInterface $output,
    QuestionHelper $helper,
    string $class
  ) {

    $data = [];
    $question = new Question("Login: ");
    $question->setValidator([$class, "emptyValidator"]);
    $data['login'] = $helper->ask($input, $output, $question);
    $question = new Question("Password: ");
    $question->setValidator([$class, "emptyValidator"]);
    $data['password'] = $helper->ask($input, $output, $question);

    return $data;
  }

}