<?php

namespace AffMarketingScripts\Commands\AbTestSpecificQuestions;

use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AffMarketingScripts\Commands\AbTestCommandBase;

class AbTestTrackerSettingsQuestions implements AbTestQuestionsInterface {

  /**
   * {@inheritdoc}
   */
  public static function ask(
    InputInterface $input,
    OutputInterface $output,
    QuestionHelper $helper,
    string $class
  ) {

    $data = [];

    // Tracker DB settings.
    $question = new Question("Keitaro db name: ", "keitaro");
    $data['database']['name'] = $helper->ask($input, $output, $question);

    $question = new Question("Keitaro server ip: ");
    $question->setValidator([self::class, "emptyValidator"]);
    $data['database']['server'] = $helper->ask($input, $output, $question);

    $question = new Question("Keitaro server port: ", 3306);
    $data['database']['port'] = $helper->ask($input, $output, $question);

    $question = new Question("Keitaro db user: ", "root");
    $data['database']['user'] = $helper->ask($input, $output, $question);

    $question = new Question("Keitaro db password: ");
    $question->setValidator([self::class, "emptyValidator"]);
    $data['database']['password'] = $helper->ask($input, $output, $question);

    return $data;
  }

}