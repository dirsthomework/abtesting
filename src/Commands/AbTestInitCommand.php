<?php

namespace AffMarketingScripts\Commands;

use AffMarketingScripts\AbTestFactory;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Yaml\Yaml;

use AffMarketingScripts\TrafficSourcePluginFactory;
use AffMarketingScripts\TrackerManagerFactory;

/**
 * Class AbTestInitCommand
 *
 * @package AffMarketingScripts\Commands
 */
class AbTestInitCommand extends AbTestCommandBase {

  /**
   * @var string
   */
  protected static $defaultName = 'ab-test:init';

  /**
   * {@inheritdoc}
   */
  protected function configure()
  {
    $this
      ->setDescription('Init traffic test')
      ->setHelp('Interactive command to define all needed configs for A/B testing');
  }

  protected function execute(InputInterface $input, OutputInterface $output) {
    $helper = $this->getHelper("question");
    $io = new SymfonyStyle($input, $output);

    $data = [];

    $question = new Question("Enter name of the test: ");
    $question->setValidator([self::class, "configNameValidator"]);
    $data['title'] = $helper->ask($input, $output, $question);
    $data['id'] = uniqid();

    $io->section("Traffic source settings:");

    // Traffic source choice.
    $question = new ChoiceQuestion(
      "Pick traffic Source: ",
      [
        TrafficSourcePluginFactory::POPADS,
        TrafficSourcePluginFactory::PLUGRUSH,
        TrafficSourcePluginFactory::POPCASH
      ]
    );
    $question->setErrorMessage("Your choice is incorrect.");
    $data['traffic_source']['name'] = $helper->ask($input, $output, $question);

    // Plugrush bw list settings.
    if ($data['traffic_source']['name'] == TrafficSourcePluginFactory::PLUGRUSH) {
      // Enter black/white list ID.
//      $question = new Question("Enter ID of the B/W List on Plugrush: ");
//      $question->setValidator([self::class, "numericValidator"]);
//      $data['traffic_source']['plugrush_bw_list_id'] = $helper->ask($input, $output, $question);
      // Enter email.
      $question = new Question("Email: ");
      $question->setValidator([self::class, "emptyValidator"]);
      $data['traffic_source']['email'] = $helper->ask($input, $output, $question);
    }

    // Api key for api request.
    $question = new Question("Api Key: ");
    $question->setValidator([self::class, "emptyValidator"]);
    $data['traffic_source']['apikey'] = $helper->ask($input, $output, $question);

    // Popads login/pass settings.
    if ($data['traffic_source']['name'] == TrafficSourcePluginFactory::POPADS) {
      $question = new Question("Login: ");
      $question->setValidator([self::class, "emptyValidator"]);
      $data['traffic_source']['login'] = $helper->ask($input, $output, $question);
      $question = new Question("Password: ");
      $question->setValidator([self::class, "emptyValidator"]);
      $data['traffic_source']['password'] = $helper->ask($input, $output, $question);
    }

    // Tracker settings.
    $io->section("Tracker settings");

    // Tracker type choice.
    $question = new ChoiceQuestion(
      "Pick tracker: ",
      [TrackerManagerFactory::KEITARO]
    );
    $question->setErrorMessage("Your choice is incorrect.");
    $data['tracker']['name'] = $helper->ask($input, $output, $question);

    // Tracker DB settings.
    $question = new Question("Keitaro db name: ", "keitaro");
    $data['tracker']['database']['name'] = $helper->ask($input, $output, $question);

    $question = new Question("Keitaro server ip: ");
    $question->setValidator([self::class, "emptyValidator"]);
    $data['tracker']['database']['server'] = $helper->ask($input, $output, $question);

    $question = new Question("Keitaro server port: ", 3306);
    $data['tracker']['database']['port'] = $helper->ask($input, $output, $question);

    $question = new Question("Keitaro db user: ", "root");
    $data['tracker']['database']['user'] = $helper->ask($input, $output, $question);

    $question = new Question("Keitaro db password: ");
    $question->setValidator([self::class, "emptyValidator"]);
    $data['tracker']['database']['password'] = $helper->ask($input, $output, $question);

    // Other settings.
    $io->section("Other settings");

    // Type of the test.
    $question = new ChoiceQuestion(
      "What type of the test is this: ",
      array_keys(AbTestFactory::getMappedAbTests()),
      0
    );
    $question->setErrorMessage("Your choice is incorrect.");
    $data['test']['name'] = $helper->ask($input, $output, $question);

    $question = new Question("Tracker campaign ID: ");
    $question->setValidator([self::class, "numericValidator"]);
    $data['test']['tracker_campaign_id'] = $helper->ask($input, $output, $question);

    // Campaign ID on traffic source.
    $question = new Question("Enter ID of the Campaign on traffic source: ");
    $question->setValidator([self::class, "numericValidator"]);
    $data['test']['campaign_id'] = $helper->ask($input, $output, $question);

    if ($data['traffic_source']['name'] == TrafficSourcePluginFactory::PLUGRUSH) {
      // Enter black/white list ID.
      $question = new Question("Enter ID of the B/W List on Plugrush: ");
      $question->setValidator([self::class, "numericValidator"]);
      $data['traffic_source']['plugrush_bw_list_id'] = $helper->ask($input, $output, $question);
    }

    // Clicks count thresold.
    $question = new Question("Trials count threshold to perform test (1000): ", 1000);
    $question->setValidator([self::class, "numericValidator"]);
    $data['test']['trials_threshold'] = $helper->ask($input, $output, $question);

    // Positive trials threshold
    $question = new Question("Positive trials count threshold to perform test (1): ", 1);
    $question->setValidator([self::class, "numericValidator"]);
    $data['test']['win_threshold'] = $helper->ask($input, $output, $question);

    // Payout param.
    $question = new Question("Offer payout ($): ");
    $question->setValidator([self::class, "numericValidator"]);
    $data['test']['payout'] = $helper->ask($input, $output, $question);

    // Min ROI (%).
    $question = new Question("Minimal ROI (20%): ", 20);
    $question->setValidator([self::class, "numericValidator"]);
    $data['test']['min_roi'] = $helper->ask($input, $output, $question);

    // Confidence level (%).
    $question = new Question("Confidence level (95%): ", 95);
    $question->setValidator([self::class, "numericValidator"]);
    $data['test']['confidence'] = $helper->ask($input, $output, $question);

    // Max traffic loss (%).
    $question = new Question("Max traffic loss (15%): ", 15);
    $question->setValidator([self::class, "numericValidator"]);
    $data['test']['max_loss'] = $helper->ask($input, $output, $question);

    // Enable config question.
    $question = new ConfirmationQuestion('Enable this config (y/n - default is no)? ', 0, "/^(y|j)/i");
    if (!$question) {
      $output->writeln("Config is going to be disable");
    }
    $data['status'] = (int) $helper->ask($input, $output, $question);

    $question = new Question("How often test should be ran - every X (minutes): ");
    $question->setValidator([self::class, "numericValidator"]);
    $data['interval'] = $helper->ask($input, $output, $question);

    // Set up last runtime
    $data['__last_runtime'] = 0;

    // Save data to config
    self::saveConfig($data);

    return 0;
  }

}