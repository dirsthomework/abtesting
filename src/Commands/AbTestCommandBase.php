<?php

namespace AffMarketingScripts\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Yaml\Yaml;

/**
 * Class AbTesCommand base class
 *
 * @TODO
 *   Replace Configs methods to separate class.
 *
 * @package AffMarketingScripts\Commands
 */
abstract class AbTestCommandBase extends Command {

  /**
   * @const string
   */
  const CONFIGS_FOLDER = __DIR__ . "/../../configs";

  /**
   * {@inheritdoc}
   */
  public function __construct($name = NULL) {
    parent::__construct($name);

    // Create configs folder.
    if (!file_exists(self::CONFIGS_FOLDER)) {
      mkdir(self::CONFIGS_FOLDER);
    }
  }

  /**
   * Numeric validator.
   *
   * @param mixed $answer
   *   Check if numeric.
   *
   * @return mixed
   *   Answer value.
   */
  static public function numericValidator($answer) {
    if (!$answer OR $answer == 0 OR !is_numeric($answer)) {
      throw new \RuntimeException(
        'Id should be numeric and not 0'
      );
    }

    return $answer;
  }

  /**
   * Empty validator.
   *
   * @param mixed $answer
   *   Check if numeric.
   *
   * @return mixed
   *   Answer value.
   */
  static public function emptyValidator($answer) {
    if (!$answer) {
      throw new \RuntimeException(
        'Enter a value'
      );
    }

    return $answer;
  }

  /**
   * Config name validator.
   *
   * @param mixed $answer
   *   Check if numeric.
   *
   * @return mixed
   *   Answer value.
   */
  static public function configNameValidator($answer) {
    $machin_name = strtolower(str_replace(" ", "_", $answer));
    if (file_exists(self::CONFIGS_FOLDER . "/" . $machin_name . ".yml")) {
      throw new \RuntimeException(
        'Enter a value'
      );
    }

    return $answer;
  }

  /**
   * Get all config files names.
   *
   * @return array
   *   Array of files.
   */
  static public function getAllParsedConfigs() {
    // Get all files in the folder
    $files = array_filter(
      scandir(self::CONFIGS_FOLDER),
      function($fname) {
        return !in_array($fname, ['.', '..']);
      }
    );

    // Parse all files.
    $parsed = [];
    foreach ($files as $key => $file) {
      $parsed[$file] = Yaml::parseFile(self::CONFIGS_FOLDER . "/" . $file);
    }

    return $parsed;
  }

  /**
   * Get Config names with status.
   *
   * @param int $status
   *   Status of the config.
   * @param array $parsed
   *   Parsed config.
   *
   * @return array
   *   Array of file names with status.
   */
  static public function getConfigNamesWithStatus($status, $parsed) {
    $choices = [];
    foreach ($parsed as $key => $config) {
      if ($config['status'] == $status) {
        $choices[] = $key;
      }
    }

    return $choices;
  }

  /**
   * Save config data.
   *
   * @param array $data
   *   Config array.
   */
  static public function saveConfig(array $data) {
    $yaml = Yaml::dump($data);
    $config_name = strtolower(str_replace(" ", "_", $data['title']));

    file_put_contents(self::CONFIGS_FOLDER . "/" . $config_name . ".yml", $yaml);
  }
}