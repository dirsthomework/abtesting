<?php

namespace AffMarketingScripts\Commands;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;

use AffMarketingScripts\TrafficSourcePluginFactory;
use AffMarketingScripts\TrackerManagerFactory;
use AffMarketingScripts\AbTestFactory;

use TelegramBot\Api\BotApi;

use Katzgrau\KLogger\Logger;

define("TELEGRAM_BOT_API", "1035324955:AAFfzU7F4vLNXhSXh_j1fiF7H3-2WRVnvuE");

/**
 * Class AbTestRunCommand
 * Runs all or specific test.
 *
 * @package AffMarketingScripts\Commands
 */
class AbTestRunCommand extends AbTestCommandBase {

  /**
   * @var \Katzgrau\KLogger\Logger
   */
  protected $logger;

  /**
   * AbTestRunCommand constructor.
   *
   * @param null $name
   */
  public function __construct($name = NULL) {
    parent::__construct($name);
    $this->logger = new Logger(__DIR__ . "/../../__localdata/logs");
  }

  /**
   * @var string
   */
  protected static $defaultName = 'ab-test:run';

  /**
   * {@inheritdoc}
   */
  protected function configure()
  {
    $this
      ->setDescription('Run traffic test')
      ->setHelp('This command runs one or more tests');
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $helper = $this->getHelper("question");

    $parsed = self::getAllParsedConfigs();

    // Get all configs with chosen status.
    $choices = self::getConfigNamesWithStatus(1, $parsed);

    if (empty($choices)) {
      $output->writeln("There are no tests to run yet.");
      return 0;
    }

    if ($input->isInteractive()) {
      $question = new ChoiceQuestion(
        "Pick configs to run: ",
        $choices
      );
      $question->setMultiselect(TRUE);
      $question->setErrorMessage("Your choice is incorrect.");
      $choices = $helper->ask($input, $output, $question);
    }

    // Init tests
    $trafficSourcePlugin = [];
    $trackerManager = [];
    foreach ($choices as $choice) {

      try {

        // Check if config can be ran.
        $lastTime = $parsed[$choice]['__last_runtime'];
        $interval = $parsed[$choice]['interval'] * 60;
        if ($lastTime + $interval >= time()) {
          $this->logger->notice(
            "Test {$parsed[$choice]["title"]} is not going to be ran - time interval has not been exceeded yet"
          );
          continue;
        }

        // Init tracker and traffic source.
        if (!isset($trafficSourcePlugin[$parsed[$choice]['traffic_source']['name']])) {
          $trafficSourcePlugin[$parsed[$choice]['traffic_source']['name']] = TrafficSourcePluginFactory::getTrafficSourcePlugin(
            $parsed[$choice]['traffic_source']
          );
        }

        // Init tracker manager.
        if (!isset($trackerManager[$parsed[$choice]['tracker']['name']])) {
          $trackerManager[$parsed[$choice]['tracker']['name']] = TrackerManagerFactory::getTrackerManager($parsed[$choice]['tracker']);
        }

        // Bot messenger.
        if (!isset($messenger)) {
          $messenger = new BotApi(TELEGRAM_BOT_API);
        }

        // Init test.
        $abtest = AbTestFactory::getAbTest(
          $trackerManager[$parsed[$choice]['tracker']['name']],
          $trafficSourcePlugin[$parsed[$choice]['traffic_source']['name']],
          $messenger,
          $parsed[$choice]['test']
        );
        $abtest->run();

        $this->logger->notice("Just ran abtest " . $parsed[$choice]["title"]);

        // Set up last run time.
        $parsed[$choice]['__last_runtime'] = time();
        self::saveConfig($parsed[$choice]);
      } catch (\Exception $e) {
        $this->logger->error($e);
      }
    }

    $this->logger->notice("One test sequence is finished");
    $this->logger->notice("==================================================");

    return 0;
  }

}