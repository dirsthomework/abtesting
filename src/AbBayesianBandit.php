<?php

namespace AffMarketingScripts;

use gburtini\Distributions\Beta;
use MathPHP\Functions\Special;

/**
 * Class AbBandit
 * Beta distribution according to current data.
 *
 * @package AffMarketingScripts
 */
class AbBayesianBandit {

  /**
   * @var array of clicks
   */
  protected $data;

  /**
   * @var \gburtini\Distributions\Beta
   */
  protected $model;

  /**
   * @var int
   */
  protected $id;

  /**
   * AbBayesianBandit constructor.
   *
   * @param array $data
   *   Array of clicks/conversion.
   * @param int bandit ID.
   */
  public function __construct(array $data, $id) {
    $this->model = new Beta(1, 1);

    $this->id = $id;

    // Update data with clicks.
    foreach ($data as $trialResult) {
      $this->updateModel($trialResult);
    }
  }

  /**
   * Get random value from Beta distribution
   *
   * @return float
   *   Random X value.
   */
  public function getSample() {
    return $this->model->rand();
  }

  /**
   * Update beta distribution model.
   *
   * @param int $clickConversion
   *   Binary result of a trial. 1 or 0.
   */
  public function updateModel($trialBinaryResult) {
    $this->model->alpha += $trialBinaryResult;
    $this->model->beta  += 1 - $trialBinaryResult;
  }

  /**
   * Get Id of the bandit.
   *
   * @return int
   *   Id of the bandit.
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Confidence interval upper.
   *
   * @param float $level
   *   Confidence level.
   *
   * @return mixed
   *   X value of beta on the upper bound.
   */
  public function getConfidenceIntervalUpper($level = 0.95) {
    $border = (1 - $level) / 2;
    $upper = 1 - ($border);
    return $this->model->icdf($upper);
  }

  /**
   * Confidence interval lower.
   *
   * @param float $level
   *   Confidence level.
   *
   * @return float
   *   X value of beta on the lower bound.
   */
  public function getConfidenceIntervalLower($level = 0.95) {
    $border = (1 - $level) / 2;
    return $this->model->icdf($border);
  }

  /**
   * Get beta model.
   *
   * @return \gburtini\Distributions\Beta
   *   Beta distribition model.
   */
  public function getModel(): \gburtini\Distributions\Beta {
    return $this->model;
  }

  /**
   * Calculates probability P (X > Y)
   *
   * X - current bandit.
   * Y - bandit passed as parameter
   *
   * @param \AffMarketingScripts\AbBayesianBandit $bandit
   *  2nd Bandit (Y)
   *
   * @return float
   *   0 - 1 value means probability of X > Y
   */
  public function calculateBetterThan(AbBayesianBandit $bandit) {
    // Calculate H function.
    return h(
      $this->model->alpha,
      $this->model->beta,
      $bandit->getModel()->alpha,
      $bandit->getModel()->beta
    );
  }

  /**
   * Calculates loss percentage of the choice between X and Y
   * in case we pick Y - where X current bandit and Y is passed bandit.
   *
   * @param \AffMarketingScripts\AbBayesianBandit $bandit
   *   2nd Bandit (Y)
   *
   * @return float
   *   0 - 1 value means percent of loss if we pick Y
   */
  public function calculateExpectedLoss(AbBayesianBandit $bandit) {
    $a = $this->model->alpha;
    $b = $this->model->beta;
    $c = $bandit->getModel()->alpha;
    $d = $bandit->getModel()->beta;
    return ( Special::beta($a + 1, $b) / Special::beta($a, $b) )
      * $this->h($a + 1, $b, $c, $d) -
      ( Special::beta($c + 1, $d) / Special::beta($c, $d) )
      * $this->h($a, $b, $c + 1, $d);
  }

  /**
   * Calculates probability P (X > Y)
   * where X = Beta (a,b) and Y Beta (c,d)
   *
   * @param int $a
   *   a parameter for X beta
   * @param int $b
   *   b paramter for X beta
   * @param int $c
   *   a parameter for Y beta
   * @param int $d
   *   b parameter for Y beta
   *
   * @return float
   *   Probability X > Y. 0 - 1 value.
   */
  protected function h($a, $b, $c, $d) {
    $sum = 0;
    for ($j = 0; $j < $c; $j++) {
      $sum += Special::beta($a + $j, $b + $d) / (($d + $j) * Special::beta(1 + $j, $d) * Special::beta($a, $b));
    }
    return 1 - $sum;
  }

}