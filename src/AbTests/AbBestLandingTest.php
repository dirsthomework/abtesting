<?php

namespace AffMarketingScripts\AbTests;

use AffMarketingScripts\AbBayesianBandit;

/**
 * Class AbBestBanditTest
 * Chose best bandit according to bayesian method.
 *
 * @package AffMarketingScripts\AbTests
 */
class AbBestLandingTest extends AbBestOfferTest {

  /**
   * {@inheritdoc}
   */
  public function run() {
    // Get all offers for the campaign.
    $landingIds = $this->trackerManager->getAllLandingIds($this->settings['tracker_campaign_id']);

    if (empty($landingIds)) {
      return;
    }

    // Get data per offer for the campaign.
    foreach ($landingIds as $id) {
      $data = $this->trackerManager->getConversionsDataForCampaign(
        $this->settings['tracker_campaign_id'],
        [],
        [],
        [],
        [$id]
      );

      // Do not perform test if its not enough data for one of the landings.
      if (count($data) < $this->settings['trials_threshold']) {
        return;
      }

      // Create bandits (offers beta distrib models) based on the data and offer id.
      $this->bandits[] = new AbBayesianBandit($data, $id);
    }

    if (empty($this->bandits)) {
      return;
    }

    // Get best bandit id and send message.
    $id = $this->getBestBanditId();
    $this->sendMessage("New lander for campaign " . $this->settings['tracker_campaign_id'] . " is $id");

    // Enable best landing  only.
    $this->trackerManager->enableLandingOnly(
      $this->settings['tracker_campaign_id'],
      $id
    );
  }

}