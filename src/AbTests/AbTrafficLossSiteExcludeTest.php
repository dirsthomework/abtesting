<?php

namespace AffMarketingScripts\AbTests;

/**
 * Class AbTrafficLossSiteExcludeTest
 * Exclude site when traffic loss is exceeded.
 *
 * @package AffMarketingScripts\AbTests
 */
class AbTrafficLossSiteExcludeTest extends AbTestBase {

  /**
   * {@inheritdoc}
   */
  public function run() {
    $exclusionsListOld = $this->getSitesExcludedCache();
    $exclusionsList = [];

    $trackerData = $this->trackerManager->getWebsitesReport($this->settings['tracker_campaign_id']);
    $sourceData  = $this->trafficSource->getReportByZones($this->settings['campaign_id']);
    $sourceData  = $this->trafficSource->normalizeReport($sourceData);

    foreach ($sourceData as $sourceRow) {
      foreach ($trackerData as $trackerRow) {

        // Calculate loss.
        if ($sourceRow['id'] == $trackerRow['id']) {
          $loss = 100 - 100 * $trackerRow["impressions"] / $sourceRow["impressions"];
          if (
            $loss > $this->settings['max_loss'] &&
            !in_array($sourceRow['id'], $exclusionsListOld) &&
            $trackerRow['sale'] + $trackerRow['lead'] < $this->settings['win_threshold'] &&
            $trackerRow["impressions"] >= $this->settings['trials_threshold']
          ) {
            $exclusionsList[] = $sourceRow['id'];
          }
        }
      }
    }

    if (!empty($exclusionsList)) {
      $this->sendMessage("Too much traff loss, campaign: {$this->settings['tracker_campaign_id']} Excluding this IDs now: " . implode(",", $exclusionsList));

      $exclusionsList = array_merge($exclusionsListOld, $exclusionsList);

      // Save list in cache
      $this->setSitesExcludedCache($exclusionsList);

      // Set blacklist on traffic source.
      $this->trafficSource->setBlacklistZones($this->settings['campaign_id'], $exclusionsList);
    }

  }

}