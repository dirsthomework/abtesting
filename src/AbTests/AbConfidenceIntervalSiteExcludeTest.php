<?php

namespace AffMarketingScripts\AbTests;

use AffMarketingScripts\AbBayesianBandit;

/**
 * Class AbConfidenceIntervalSiteExcludeTest
 *
 * @package AffMarketingScripts\AbTests
 */
class AbConfidenceIntervalSiteExcludeTest extends AbTestBase {

  /**
   * @var string
   */
  protected $websitesExludeFolder = __DIR__ . "/../../__localdata/website_exclude";

  /**
   *  {@inheritdoc}
   */
  public function run() {

    // Create folder if doesn't exists
    if (!file_exists($this->websitesExludeFolder)) {
      mkdir($this->websitesExludeFolder, 0777, true);
    }

    // Get previously saved data.
    $exclusionsListOld = $this->getSitesExcludedCache();
    $exclusionsList = [];

    // Get data by all websites.
    $data = $this->trackerManager->getWebsitesReport($this->settings['tracker_campaign_id']);
    foreach ($data as $row) {

      // Don't waste resources if there is not enough clicks.
      if ($this->settings['trials_threshold'] > $row['impressions']) {
        continue;
      }

      /**
       * CHECK IF ZONE IN BLACKLIST ALREADY.
       */
      if (in_array($row['id'], $exclusionsListOld)) {
        continue;
      }

      $clickCost = $row['cost'] / $row['impressions'];

      // Payout will be taken from config if there are no conversions.
      $payout = ($row['sale'] + $row['lead']) == 0 ?
        $this->settings['payout'] :
        ($row['lead_revenue'] + $row['revenue']) / ($row['sale'] + $row['lead']);

      $minRate = ($this->settings['min_roi'] / 100 + 1) * $clickCost / $payout * 100;

      // First check if we have profit at list from this site.
      if ($row['lead_revenue'] + $row['revenue'] > $row['cost']) {
        continue;
      }

      // Conversions/clicks data.
      $websiteClicks = $this->trackerManager->getConversionsDataForCampaign(
        $this->settings['tracker_campaign_id'],
        [$row['id']]
      );

      // Create beta distrib model to retrieve confidence interval upper bound.
      $bandit = new AbBayesianBandit($websiteClicks, $row['id']);
      if ($minRate > 100 * $bandit->getConfidenceIntervalUpper($this->settings['confidence'] / 100)) {
        $exclusionsList[] = $row['id'];
      }

//      $out = "RATE/IMPRESSIONS:" . $minRate . "/" . $row['impressions'];
//      $out .= PHP_EOL;
//      $out .= "ID:" . $row['id'];
//      $out .= PHP_EOL;
//      $out .= "UPPER BOUND ({$this->settings['confidence']}%)" . 100 * $bandit->getConfidenceIntervalUpper($this->settings['confidence'] / 100) . PHP_EOL;
//      $out .= "+++++++" . PHP_EOL;
//
//      $this->sendMessage($out);
    }

    if (!empty($exclusionsList)) {
      $this->sendMessage("Campaign: {$this->settings['tracker_campaign_id']} Excluding this IDs now: " . implode(",", $exclusionsList));

      $exclusionsList = array_merge($exclusionsListOld, $exclusionsList);

      // Save list in cache
      $this->setSitesExcludedCache($exclusionsList);

      // Set blacklist on traffic source.
      $this->trafficSource->setBlacklistZones($this->settings['campaign_id'], $exclusionsList);
    }
  }

}