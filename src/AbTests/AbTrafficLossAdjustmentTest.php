<?php

namespace AffMarketingScripts\AbTests;

/**
 * Class AbTrafficLossAdjustmentTest
 *
 * @package AffMarketingScripts\AbTests
 */
class AbTrafficLossAdjustmentTest extends AbTestBase {

  /**
   * {@inheritdoc}
   */
  public function run() {
    $sourceReport  = $this->trafficSource->getReportByZones($this->settings['campaign_id']);
    $trackerReport = $this->trackerManager->getWebsitesReport($this->settings['tracker_campaign_id']);

    $trafficSourceSum = 0;
    $excluded = $this->getSitesExcludedCache();
    foreach ($this->trafficSource->normalizeReport($sourceReport) as $rows) {
      if (!in_array($rows['id'], $excluded)) {
        $trafficSourceSum += $rows['impressions'];
      }
    }

    // Check threshold to perform test.
    if ($trafficSourceSum < $this->settings['trials_threshold']) {
      return;
    }

    $keitaroSourceSum = 0;
    foreach ($trackerReport as $rows) {
      if (!in_array($rows['id'], $excluded)) {
        $keitaroSourceSum += $rows['impressions'];
      }
    }

    $percent = 100 - 100 * $keitaroSourceSum / $trafficSourceSum;

    $this->sendMessage("Campaign:{$this->settings['tracker_campaign_id']} New traffic loss is $percent%");

    $this->trackerManager->updateTrafficLoss($this->settings['tracker_campaign_id'], $percent);
  }
}