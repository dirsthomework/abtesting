<?php

namespace AffMarketingScripts\AbTests;

use AffMarketingScripts\TrafficSourcePlugins\TrafficSourcePluginInterface;
use AffMarketingScripts\TrackerManagers\TrackerManagerInterface;

use TelegramBot\Api\BotApi;

/**
 * Interface class AbTestInterface
 * Base interface for all ABtests.
 *
 * @package AffMarketingScripts\AbTests
 */
abstract class AbTestBase {

  /**
   * @var string
   */
  protected $websitesExludeFolder = __DIR__ . "/../../__localdata/website_exclude";

  /**
   * @var \AffMarketingScripts\TrafficSourcePlugins\TrafficSourcePluginInterface
   */
  protected $trafficSource;

  /**
   * @var \AffMarketingScripts\TrackerManagers\TrackerManagerInterface
   */
  protected $trackerManager;

  /**
   * @var array
   */
  protected $settings;

  /**
   * @var \TelegramBot\Api\BotApi
   */
  protected $messenger;

  /**
   * @var int
   */
  protected $messengerChatId = 294866280;

  /**
   * AbTestInterface constructor.
   *
   * @param \AffMarketingScripts\TrackerManagers\TrackerManagerInterface $trackerManager
   *   Tracker Manager.
   * @param \AffMarketingScripts\TrafficSourcePlugins\TrafficSourcePluginInterface $trafficSource
   *   Traffic source plugin.
   * @param array $settings
   *   Test dependent settings.
   */
  public function __construct(
    TrackerManagerInterface $trackerManager,
    TrafficSourcePluginInterface $trafficSource,
    BotApi $messenger,
    array $settings
  ) {
    $this->trafficSource = $trafficSource;
    $this->trackerManager = $trackerManager;
    $this->settings = $settings;
    $this->messenger = $messenger;
  }

  /**
   * Get sites excluded values stored locally.
   *
   * @return array
   *   Sites excluded list.
   */
  public function getSitesExcludedCache() {
    $className = (new \ReflectionClass($this->trafficSource))->getShortName();
    // Get previously saved data.
    $sitesExcludeName = "/" . strtolower($className) . $this->settings['tracker_campaign_id'];

    if (file_exists($this->websitesExludeFolder . $sitesExcludeName)) {
      $fileContents = file_get_contents(
        $this->websitesExludeFolder . $sitesExcludeName
      );
    } else {
      $fileContents = false;
    }
    return $fileContents ? explode(PHP_EOL, $fileContents) : [];
  }

  /**
   * Set sites excluded values stored locally.
   *
   * @param array $exclusionsList
   *   Sites excluded list.
   */
  public function setSitesExcludedCache(array $exclusionsList) {
    $className = (new \ReflectionClass($this->trafficSource))->getShortName();
    // Set cache.
    $sitesExcludeName = "/" . strtolower($className) . $this->settings['tracker_campaign_id'];

    $exclusionsList = array_unique($exclusionsList);
    file_put_contents(
       $this->websitesExludeFolder . $sitesExcludeName,
      implode(PHP_EOL, $exclusionsList)
    );
  }

  /**
   * Send message to telegram bot.
   *
   * @param string $msg
   *   Message text.
   */
  protected function sendMessage($msg) {
    if (isset($this->settings['message']) && $this->settings['message']) {
      $this->messenger->sendMessage(
        $this->messengerChatId,
        $msg
      );
    }
  }

  /**
   * Run current test.
   */
  public abstract function run();

}