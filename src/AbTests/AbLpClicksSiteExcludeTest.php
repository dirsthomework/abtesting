<?php

namespace AffMarketingScripts\AbTests;

/**
 * Class AbLpClicksSiteExcludeTest
 * Exclude landing on no clicks after X visits.
 *
 * @package AffMarketingScripts\AbTests
 */
class AbLpClicksSiteExcludeTest extends AbTestBase {

  /**
   * {@inheritdoc}
   */
  public function run() {
    $clicks = $this->trackerManager->getWebsitesClicksByLandings($this->settings['tracker_campaign_id']);

    // Get previously saved data.
    $exclusionsListOld = $this->getSitesExcludedCache();
    $exclusionsList = [];

    // Iterate websites
    foreach ($clicks as $row) {
      // Do nothing if there are not enough clicks.
      if ($row['impressions'] < $this->settings['trials_threshold']) {
        continue;
      }

      // Exclude site if lp clicks count less then setted threshold.
      if (
        $row["lp_clicks"] < $this->settings['win_threshold']
        &&
        !in_array($row['id'], $exclusionsListOld)
      ) {
        $exclusionsList[] = $row['id'];
      }

    }

    if (!empty($exclusionsList)) {

      $this->sendMessage("No lp clicks, campaign: {$this->settings['tracker_campaign_id']} Excluding this IDs now: " . implode(",", $exclusionsList));

      $exclusionsList = array_merge($exclusionsListOld, $exclusionsList);

      // Set sites excluded cache.
      $this->setSitesExcludedCache($exclusionsList);

      // Set blacklist on traffic source.
      $this->trafficSource->setBlacklistZones($this->settings['campaign_id'], $exclusionsList);
    }
  }

}