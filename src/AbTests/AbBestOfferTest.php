<?php

namespace AffMarketingScripts\AbTests;

use AffMarketingScripts\AbBayesianBandit;

/**
 * Class AbBestBanditTest
 * Chose best bandit according to bayesian method.
 *
 * @package AffMarketingScripts\AbTests
 */
class AbBestOfferTest extends AbTestBase {

  /**
   * @var array of bandits.
   */
  protected $bandits = [];

  /**
   * {@inheritdoc}
   */
  public function run() {
    // Get all offers for the campaign.
    $offerIds = $this->trackerManager->getAllOfferIds($this->settings['tracker_campaign_id']);

    if (empty($offerIds)) {
      return;
    }

    // Get data per landing for the campaign.
    foreach ($offerIds as $id) {
      $data = $this->trackerManager->getConversionsDataForCampaign(
        $this->settings['tracker_campaign_id'],
        [],
        [],
        [$id]
      );

      // Do not perform test if its not enough data for one of the offers.
      if (count($data) < $this->settings['trials_threshold']) {
        return;
      }

      // Create bandits (offers beta distrib models) based on the data and offer id.
      $this->bandits[] = new AbBayesianBandit($data, $id);
    }

    if (empty($this->bandits)) {
      return;
    }

    $id = $this->getBestBanditId();
    $this->sendMessage("New offer for campaign " . $this->settings['tracker_campaign_id'] . " is $id");

    // Enable best offer only.
    $this->trackerManager->enableOfferOnly(
      $this->settings['tracker_campaign_id'],
      $id
    );

  }

  /**
   * Get best bandit Id.
   *
   * @return int
   *   Best best bandit Id.
   */
  protected function getBestBanditId() {
    $maxSample = -1;
    $bestBandit = null;

    foreach ($this->bandits as $bandit) {
      $sample = $bandit->getSample() * $this->getDiffKoeffByPayout($bandit->getId());

      $this->sendMessage("Sample {$sample} for {$bandit->getId()}");

      if ($sample > $maxSample) {
        $bestBandit = $bandit;
        $maxSample = $sample;
      }
    }

    return $bestBandit->getId();
  }

  /**
   * Get Normalize koefficient by payout.
   * Takes maximum payout and calc difference between current payout and max payout.
   *
   * We want to compare offers with different payouts.
   * So we pretend that payouts are equal to max payout
   * and change conversion/click rates proportionally to
   * payouts difference with this koefficient. KOEFF = currentPayout/maxPayout
   *
   * 2 offers payouts:
   *
   * 1 - 0.05
   * 2 - 0.25
   *
   * CVR
   * 1 - 5%
   * 2 - 3%
   *
   * to compare we get diff koefficient 0.05/025 = 0.2
   * multiple CVR of 1st times KOEFF: 5*0.2 = 1
   * Now we compare 2 offers with payout 0.25 and CVRS
   * 1 - 1%
   * 2 - 3%
   *
   * Which means 2nd offer is still better even the actual conversion rate (5%) of 1st is better.
   *
   * @param int $banditId
   *   Current bandit ID.
   *
   * @return float
   *   Return koeff for current bandit sample.
   */
  protected function getDiffKoeffByPayout($banditId) {
    // If payout is array then calculate
    if (is_array($this->settings['payout'])) {
      $maxPayout = max($this->settings['payout']);
      return $this->settings['payout'][$banditId] / $maxPayout;
    } else {
      // if payout is one then koefficient is 1 (no change in conversion/click rate).
      return 1.0;
    }
  }

}