<?php

namespace AffMarketingScripts\TrackerManagers;

/**
 * Interface TrackerManager
 *
 * @package AffMarketingScripts\Keitaro
 */
interface TrackerManagerInterface {

  /**
   * Update traffic lost.
   *
   * @param int $campaignId
   *   Campaign Id in tracker.
   * @param int $lost
   *   % of Traffic loss.
   */
  public function updateTrafficLoss($campaignId, $lost);

  /**
   * Enable one offer and stream 100% of traffic to it.
   *
   * @param int $campaignId
   *   Campaign ID in tracker.
   * @param int $offerId
   *   Offer ID in tracker for campaign.
   */
  public function enableOfferOnly($campaignId, $offerId);

  /**
   * Enable one landing and stream 100% of traffic to it.
   *
   * @param int $campaignId
   *   Campaign ID in tracker.
   * @param int $landingId
   *   Landing ID in tracker for campaign.
   */
  public function enableLandingOnly($campaignId, $landingId);

  /**
   * Get report for clicks grouped by websites.
   *
   * @param int $campaignId
   *   Tracker camaign Id.
   *
   * @return array
   *   Report array.
   */
  public function getWebsitesReport($campaignId);

  /**
   * Get conversions in the clicks sequence by campaign.
   * In the ascending order by date.
   *
   * @param int $campaignId
   *   Campaign ID in the tracker.
   * @param array $sitesIds
   *   Sites ids to include in the report. Leave empty to include all.
   * @param array $sitesIdsExclude
   *   Sites ids to exclude from report.
   * @param array $offersIds
   *   Offer ids to include in the report.
   * @param array $landingsIds
   *   Landing ids to include in the report.
   * @param string $status
   *   Status of the conversion to get. (sale,lead,rejected,confirmed etc)
   *
   * @return array
   *   Conversions on every click
   */
  public function getConversionsDataForCampaign(
    $campaignId,
    array $sitesIds = [],
    array $sitesIdsExclude = [],
    array $offersIds = [],
    array $landingsIds = [],
    $status = null
  );

  /**
   * Get all offer IDS.
   *
   * @param int $campaignId
   *   Campaign ID.
   *
   * @return array
   *   Offers IDS.
   */
  function getAllOfferIds($campaignId);

  /**
   * Get all landing IDS.
   *
   * @param int $campaignId
   *   Campaign ID.
   *
   * @return array
   *   Landing IDS.
   */
  function getAllLandingIds($campaignId);

  /**
   * Retrieve number of clicks grouped by websites.
   *
   * @param int $campaignId
   *   Campaign ID.
   *
   * @return array
   *   Report array.
   */
  function getWebsitesClicksByLandings($campaignId);

}