<?php

namespace AffMarketingScripts\TrackerManagers;

use ParagonIE\EasyDB\Factory;

/**
 * Class KeitaroManager
 * Perform operations with keitaro tracker.
 *
 * @package AffMarketingScripts\TrackerManager
 */
class KeitaroManager implements TrackerManagerInterface {

  /**
   * @var \ParagonIE\EasyDB\EasyDB
   */
  protected $connection;

  /**
   * Statuses columns.
   */
  const SALE = "is_sale";
  const LEAD = "is_lead";
  const REJECTED = "is_rejected";

  /**
   * KeitaroManager constructor.
   * Init Keitaro connection.
   *
   * @param string $db
   *   DB name.
   * @param string $server
   *   DB server.
   * @param int $port
   *   DB port.
   * @param string $login
   *   DB user login.
   * @param $password
   *   DB user password.
   */
  public function __construct(
    $db,
    $server,
    $port,
    $login,
    $password
  ) {

    $this->connection = Factory::fromArray([
      "mysql:host=$server;dbname=$db;port=$port",
      $login,
      $password
    ]);

  }

  /**
   * {@inheritdoc}
   */
  public function updateTrafficLoss($campaignId, $lost) {
    $this->connection->update('keitaro_campaigns', [
        'traffic_loss' => $lost
      ],
      ['id' => $campaignId]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function enableOfferOnly($campaignId, $offerId) {
    // Set 1 offer as 100% share while others to 0.
    $this->connection->run(
      "UPDATE keitaro_stream_offer_associations a 
      LEFT JOIN keitaro_streams b 
        ON a.stream_id = b.id SET a.share = 
        CASE a.offer_id 
          WHEN ? THEN 100
          ELSE 0 
        END
      WHERE b.campaign_id = ?",
      $offerId,
      $campaignId
    );
  }

  /**
   * {@inheritdoc}
   */
  public function enableLandingOnly($campaignId, $landingId) {
    // Set 1 landing as 100% share while others to 0.
    $this->connection->run(
      "UPDATE keitaro_stream_landing_associations a 
      LEFT JOIN keitaro_streams b 
        ON a.stream_id = b.id SET a.share = 
        CASE a.landing_id 
          WHEN ? THEN 100
          ELSE 0 
        END
      WHERE b.campaign_id = ?",
      $landingId,
      $campaignId
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getWebsitesReport($campaignId) {
    $sql = "SELECT s.value id,
      count(distinct(c.visitor_id)) impressions,
      sum(" . self::REJECTED . ") rejected,
      sum(" . self::SALE . ") sale,
      sum(" . self::LEAD .") lead,
      sum(sale_revenue) revenue,
      sum(lead_revenue) lead_revenue,
      sum(cost) cost 
      from keitaro_clicks c 
      LEFT JOIN keitaro_visitors v ON c.visitor_id = v.id
      LEFT JOIN keitaro_ref_sources s ON s.id = c.source_id
      where c.campaign_id = ? group by c.source_id ORDER BY datetime ASC";
    return $this->connection->run($sql, $campaignId);
  }

  /**
   * {@inheritdoc}
   */
  public function getConversionsDataForCampaign(
    $campaignId,
    array $sitesIds = [],
    array $sitesIdsExclude = [],
    array $offersIds = [],
    array $landingsIds = [],
    $status = null
  ) {
      // Select conversions number according to status and exclusions.
      $col = $status ? $status : "(case when is_lead = 0 then is_sale else is_lead end)";
      $sql = "SELECT $col
              as conversions FROM keitaro_clicks c
              LEFT JOIN keitaro_ref_sources s ON s.id = c.source_id
              WHERE campaign_id = ?";

      $params[] = $campaignId;

      // Assign filters
      if (!empty($sitesIds)) {
        $sql .= " AND s.value IN (?)";
        $params[] = implode(",", $sitesIds);
      }
      if (!empty($sitesIdsExclude)) {
        $sql .= " AND s.value NOT IN (?)";
        $params[] = implode(",", $sitesIdsExclude);
      }
      if (!empty($offersIds)) {
        $sql .= " AND offer_id IN (?)";
        $params[] = implode(",", $offersIds);
      }
      if (!empty($landingsIds)) {
        $sql .= " AND c.landing_id IN (?)";
        $params[] = implode(",", $landingsIds);
      }

      $sql .= " ORDER BY datetime ASC";

      $data = $this->connection->safeQuery(
        $sql,
        $params
      );

      return array_map(function ($element) { return $element['conversions']; }, $data);
  }

  /**
   * {@inheritdoc}
   */
  function getAllOfferIds($campaignId) {
    $sql = "SELECT distinct(offer_id) FROM keitaro_clicks where campaign_id = ?";
    return array_map(function ($element) { return $element['offer_id']; } , $this->connection->run($sql, $campaignId));
  }

  /**
   * {@inheritdoc}
   */
  function getAllLandingIds($campaignId) {
    $sql = "SELECT distinct(landing_id) FROM keitaro_clicks where campaign_id = ?";
    return array_map(function ($element) { return $element['landing_id']; } , $this->connection->run($sql, $campaignId));
  }

  /**
   * {@inheritdoc}
   */
  function getWebsitesClicksByLandings($campaignId) {
    $sql = "SELECT 
      count(*) impressions,
      sum(landing_clicked) lp_clicks,
      s.value id
      FROM keitaro.keitaro_clicks c
      LEFT JOIN keitaro_ref_sources s ON s.id = c.source_id
      WHERE campaign_id = ? group by c.source_id";

    return $this->connection->run($sql, $campaignId);
  }

}