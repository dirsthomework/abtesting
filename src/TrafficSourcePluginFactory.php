<?php

namespace AffMarketingScripts;

use AffMarketingScripts\TrafficSourcePlugins\PlugrushPlugin;
use AffMarketingScripts\TrafficSourcePlugins\PopadsPlugin;
use AffMarketingScripts\TrafficSourcePlugins\PopcashPlugin;

/**
 * Class TrafficSourcePluginFactory
 * Returns Traffic Source interface.
 *
 * @package AffMarketingScripts
 */
class TrafficSourcePluginFactory {

  /**
   * @const Traffic Sources.
   */
  const POPADS = "POPADS";
  const PLUGRUSH = "PLUGRUSH";
  const POPCASH = "POPCASH";

  /**
   * Return traffic plugin object.
   *
   * @param array $trafficSourseData
   *   Traffic source Data.
   *
   * @return \AffMarketingScripts\TrafficSourcePlugins\TrafficSourcePluginInterface
   *   Class object which implements traffic source interface.
   *
   * @throws \Exception
   *   On plugin class not found.
   */
  public static function getTrafficSourcePlugin(array $trafficSourseData) {
    switch ($trafficSourseData['name']) {
      case self::POPADS:
        $popads = new PopadsPlugin($trafficSourseData["apikey"]);
        $popads->login($trafficSourseData['login'], $trafficSourseData['password']);
        return $popads;
      case self::POPCASH:
        return new PopcashPlugin($trafficSourseData["apikey"]);
      case self::PLUGRUSH:
        return new PlugrushPlugin($trafficSourseData["email"], $trafficSourseData["apikey"], $trafficSourseData["plugrush_bw_list_id"]);
      default:
        throw new \Exception("Couldn't init TrafficSource plugin class - config is invalid.");
    }
  }

}