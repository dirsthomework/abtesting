<?php

namespace AffMarketingScripts\TrafficSourcePlugins;

use GuzzleHttp\Client;

/**
 * Class PopcashPlugin
 *
 * Using popcash standard api.
 *
 * @package AffMarketingScripts\TrafficSourcePlugins
 */
class PopcashPlugin implements TrafficSourcePluginInterface {

  /**
   * @var string
   */
  protected $baseUrl = "https://api.popcash.net";

  /**
   * @var string
   */
  protected $reportEndpoint = "/reports/advertiser/campaign/";

  /**
   * @var string
   */
  protected $reportDownloadEndpoint = "/reports/advertiser/download";

  /**
   * @var string
   */
  protected $zonesEditEndpoint = "/campaigns/{ID}/targeting";

  /**
   * @var string
   */
  protected $apiKey;

  /**
   * PopcashPlugin constructor.
   *
   * @param string $apiKey
   *   Define api key.
   */
  public function __construct($apiKey) {
    $this->apiKey = $apiKey;
  }

  /**
   * Only 91 days report available.
   * {@inheritdoc}
   */
  public function getReportByZones($campaignId, $dateStart = 0, $dateEnd = 0) {
    $data = [];

    // Get max interval 91 days since today.
    if ($dateEnd == 0) {
      $data['endDate'] = date("m/d/Y");
      $data['startDate'] = date("m/d/Y", strtotime('-91 days', time()));
    } else {
      // Get Max interval since end date.
      $data['endDate'] = date("m/d/Y", $dateEnd);
      $data['startDate'] = date("m/d/Y", strtotime('-91 days', $dateEnd));
    }

    // Website grouping.
    $data['reportType'] = 2;

    // Get report in queue and check on its status.
    $client = new Client();
    $response = $client->post(
      $this->baseUrl . $this->reportEndpoint . $campaignId,
      [
        "body" => json_encode($data),
        "query" => ["apikey" => $this->apiKey],
        "headers" => ["Content-Type" => "application/json"]
      ]
    );

    if ($response->getStatusCode() != 200) {
      throw new \Exception("Couldn't retrieve download token for report.");
    }

    // Get data and check on token.
    $data = json_decode($response->getBody()->getContents(), true);
    if (!isset($data['token'])) {
      throw new \Exception("Couldn't find download token for report.");
    }

    // Now download the report according to token.
    $response = $client->post(
      $this->baseUrl . $this->reportDownloadEndpoint,
      [
        "body" => json_encode(["token" => $data['token']]),
        "query" => ["apikey" => $this->apiKey],
        "headers" => ["Content-Type" => "application/json"]
      ]
    );

    // Check if report is ready. And repeat the operation after 3 seconds.
    if ($response->getStatusCode() == 404) {
      // Report is not ready yet.
      sleep(3);
      return $this->getReportByZones($campaignId, $dateStart, $dateEnd);
    }

    // Get csv report.
    $csvReport = $response->getBody()->getContents();
    $csvReport = array_map('str_getcsv', explode(PHP_EOL, $csvReport));

    // Normalize array keys for every row as associative array of headers.
    $headers = array_shift($csvReport);
    foreach ($csvReport as $r => $oneRow) {
      foreach ($oneRow as $key => $col) {
        $csvReport[$r][$headers[$key]] = (float) $col;
        unset($csvReport[$r][$key]);
      }
    }

    return $csvReport;
  }

  /**
   * {@inheritdoc}
   */
  public function setBlacklistZones($campaignId, array $zonesList) {

    $data = [
      "append" => "FALSE",
      "siteTargeting" => 1, // EXCLUDE
      "websitesIds" => $zonesList
    ];

    $client = new Client();
    $response = $client->post(
      $this->baseUrl . str_replace("{ID}", $campaignId, $this->zonesEditEndpoint),
      [
        "body" => json_encode($data),
        "query" => ["apikey" => $this->apiKey],
        "headers" => ["Content-Type" => "application/json"]
      ]
    );

    if ($response->getStatusCode() != 200) {
      throw new \Exception("Couldn't change campaign blacklist.");
    }

  }

  /**
   * {@inheritdoc}
   */
  public function normalizeReport(array $report) {
    $all = [];
    foreach ($report as $row) {
      $all[] = [
        'impressions' => $row['Visits'],
        'cost' => $row['Spent'],
        'id' => $row['Website'],
        'conversions' => $row['Conversions'],
        'payout' => $row['Payout']
      ];
    }

    return $all;
  }

}