<?php

namespace AffMarketingScripts\TrafficSourcePlugins;

/**
 * Interface TrafficSourcePluginInterface
 *
 * @package AffMarketingScripts\TrafficSourcePlugins
 */
interface TrafficSourcePluginInterface {

  /**
   * Get visits report from sources grouped by websites IDs
   *
   * @param int $campaignId
   *   Campaign ID in traffic source.
   * @param int $dateStart
   *   Date start timestamp.
   * @param int $dateEnd
   *   Date end timestamp.
   *   if Passed 0 then total report will be provided.
   *
   * @return array
   *   Report as array.
   */
  public function getReportByZones($campaignId, $dateStart = 0, $dateEnd = 0);

  /**
   * Sets blacklist with the zones.
   *
   * @param int $campaignId
   *   Campaign ID in traffic source.
   * @param array $zonesList
   *   Websites IDs list. All zones must be passed with included ones already.
   */
  public function setBlacklistZones($campaignId, array $zonesList);

  /**
   * Normalizes current traffic source report to generic one.
   *
   * ReportRowFields = [
   *  'impressions',
   *  'cost',
   *  'id',
   *  'conversions',
   *  'conversion_value'
   * ]
   *
   * @param array $report
   *   Report array.
   *
   * @return array
   *   Report as array.
   */
  public function normalizeReport(array $report);
}
