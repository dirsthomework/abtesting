<?php

namespace AffMarketingScripts\TrafficSourcePlugins;

use GuzzleHttp\Client;

/**
 * Class PlugrushPlugin
 * Using standard plugrush api.
 *
 * @package AffMarketingScripts\TrafficSourcePlugins
 */
class PlugrushPlugin implements TrafficSourcePluginInterface {

  /**
   * @var string
   */
  protected $baseUrl = "https://admin.plugrush.com/api/v2";

  /**
   * @var string
   */
  protected $reportEndpoint = "/stats/advertiser/websites";

  /**
   * @var string
   */
  protected $zonesEditEndpoint = "/wblists/{ID}/add/websites";

  /**
   * @var string
   */
  protected $apiKey;

  /**
   * @var string
   */
  protected $hash;

  /**
   * @var string
   */
  protected $mail;

  /**
   * @var int
   */
  protected $bwListId;

  /**
   * @var int
   */
  protected $timestamp;

  /**
   * Init apikey and email to property and calculate 1hour hash.
   *
   * @param string $email
   *   Account email.
   * @param string $apiKey
   *   Account apiKey.
   * @param int $bwList
   *   Black white list id.
   */
  public function __construct($email, $apiKey, $bwListId) {
    $this->timestamp = time();
    $this->hash = hash("sha256",$apiKey . "|" .  $this->timestamp . "|" . $email);
    $this->mail = $email;
    $this->apiKey = $apiKey;
    $this->bwListId = $bwListId;
  }

  /**
   * {@inheritdoc}
   */
  public function getReportByZones($campaignId, $dateStart = 0, $dateEnd = 0) {

    /**
    {
      "cost": 1.3919999999999297,
      "uniques": "3480",
      "conversions": "0",
      "conversions_amount": 0,
      "conversion_rate": 0,
      "costPerClick": 0.0003999999999999798,
      "website": "xxx.porndult.com",
      "id": 328266
    },
    */

    // if dateStart is 0 then take 1year ago data.
    $query = [
      "email" => $this->mail,
      "hash"  => $this->hash,
      "timestamp" => $this->timestamp,
      "campaigns" => $campaignId,
      "fromDate" => $dateStart == 0 ? date("Y-m-d", strtotime("-365 days", time())) : date("Y-m-d", $dateStart),
      "limit" => 0,
      "offset" => 0
    ];

    // If end date is 0 then use todays date.
    if($dateEnd == 0) {
      $query['toDate'] = date("Y-m-d", time());
    }

    // Make request to get the report.
    $client = new Client();
    $response = $client->get(
      $this->baseUrl . $this->reportEndpoint,
      [
        "query" => $query,
        "headers" => ["Content-Type" => "application/json"]
      ]
    );

    if ($response->getStatusCode() != 200) {
      throw new \Exception("Couldn't get report.");
    }

    // Decode and return report data.
    $data = json_decode($response->getBody()->getContents(), true);
    if (isset($data['data'])) {
      return $data['data'];
    } else {
      throw new \Exception("There is no data in get report response." . var_export($data, true));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setBlacklistZones($campaignId, array $zonesList) {
    $query = [
      "email" => $this->mail,
      "hash"  => $this->hash,
      "timestamp" => $this->timestamp
    ];

    // Plugrush provides blacklists as separate entity. So its needed to get list id by campaign ID.
    $client = new Client();
    $endpoint = str_replace("{ID}", $this->bwListId, $this->zonesEditEndpoint);
    $response = $client->post(
      $this->baseUrl . $endpoint,
      [
        "body" => json_encode($zonesList),
        "query" => $query,
        "headers" => ["Content-Type" => "application/json"]
      ]
    );

    if ($response->getStatusCode() != 200) {
      throw new \Exception("Couldn't change campaign blacklist.");
    }

  }

  /**
   * Use campaign Id to retrieve plugrush sites list ID.
   * - Because Plugrush provides blacklists as separate entity.
   *
   * @TODO
   *   Plugrush campaign ID is treated  as list id now,
   *   but list Id should be taken by campaign ID.
   *
   * @param $campaignId
   *
   * @return mixed
   */
  protected function getWBListByCampaignId($campaignId) {
    return $this->bwListId;
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeReport(array $report) {
    $all = [];
    foreach ($report as $row) {
      $all[] = [
        'impressions' => $row['uniques'],
        'cost' => $row['cost'],
        'id' => $row['id'],
        'conversions' => $row['conversions'],
        'payout' => $row['conversions_amount']
      ];
    }

    return $all;
  }

}