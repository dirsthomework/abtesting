<?php

namespace AffMarketingScripts\TrafficSourcePlugins;

define('MAX_FILE_SIZE', 1000000);

use GuzzleHttp\Client;
use Sunra\PhpSimple\HtmlDomParser;

/**
 * Class PopadsPlugin
 *
 * Popads doesn't provide api for editing the campaign.
 * We are using scrape methods for that.
 *
 * @package AffMarketingScripts\TrafficSourcePlugins
 */
class PopadsPlugin implements TrafficSourcePluginInterface {

  /**
   * @var string
   */
  protected $baseUrl = "https://www.popads.net";

  /**
   * @var string
   */
  protected $didChallengeUrl = "/safe/challenge.php";

  /**
   * @var string
   */
  protected $dashboardUrl = "/users/dashboard";

  /**
   * @var string
   */
  protected $loginUrl = "/users/login";

  /**
   * @var string
   */
  protected $reportEndpoint = "/api/report_advertiser";

  /**
   * @var string
   */
  protected $campaignEditUrl = "/campaigns/edit/";

  /**
   * @var string
   */
  protected $binPath = __DIR__ . "/../../bin/popads";

  /**
   * @var string
   */
  protected $sessionsPath = __DIR__ . "/../../__localdata/popads_sessions";

  /**
   * @var string
   */
  protected $siteExcludeUrl = "/reports/wst/exclude";

  /**
   * @var bool
   */
  protected $loggedIn = FALSE;

  /**
   * @var \GuzzleHttp\Cookie\CookieJar
   */
  protected $cookieJar = NULL;

  /**
   * @var string
   */
  protected $apiKey;

  /**
   * PopcashPlugin constructor.
   *
   * @param string $apiKey
   *   Define api key.
   */
  public function __construct($apiKey) {

    // Create local folder
    if (!file_exists($this->sessionsPath)) {
      mkdir($this->sessionsPath, 0777, true);
    }

    $this->apiKey = $apiKey;
  }

  /**
   *  Report data example.
      {
      "impressions": 2,
      "history_hostname": "tr9.trds.tomksoft.net",
      "history_session_time_zone": "Europe/London",
      "history_system_time_zone": "CDT",
      "cost": 0.001222,
      "website_id": "3298",
      "conversion_count": 0,
      "conversion_value": 0,
      "conversion_profit": -0.001222,
      "conversion_roi": -1,
      "conversion_rate": "-",
      "conversion_ecpa": "-"
      },
   * {@inheritdoc}
   */
  public function getReportByZones($campaignId, $dateStart = 0, $dateEnd = 0) {

    // Set up parameters.
    $params = [];

    if ($dateEnd == 0) {
      $params['quick'] = "total";
    } else {
      $params['start'] = $dateStart;
      $params['end'] = $dateEnd;
    }

    $params['groups'] = "website";
    $params['campaigns'] = $campaignId;

    $params['key'] = $this->apiKey;
    $params['zone'] = date_default_timezone_get();

    // Get report.
    $client = new Client();
    $response = $client->post(
      $this->baseUrl . $this->reportEndpoint,
      [
        'query' => $params,
        'headers' => ["Accept" => "application/json"]
      ]
    );

    // Check on success.
    if ($response->getStatusCode() == 200) {

      // Only if rows element exists.
      $data = json_decode($response->getBody()->getContents(), true);
      if (isset($data['rows']) && !empty($data['rows'])) {
        return $data['rows'];
      } else {
        return [];
      }
    } else {
      throw new \Exception("Couldn't get report.");
    }

  }

  /**
   * {@inheritdoc}
   */
  function setBlacklistZones($campaignId, array $zonesList) {
    // Should be logged in first.
    if (!$this->loggedIn) {
      throw new \Exception("Login first.");
    }

    // Exclude all zones via http requests.
    foreach ($zonesList as $zone) {
      $client = new Client(["cookies" => TRUE]);
      $response = $client->get(
        $this->baseUrl . $this->siteExcludeUrl,
        [
          "cookies" => $this->cookieJar,
          'query' => [
            "cid" => $campaignId,
            "wid" => $zone
          ]
        ]
      );

      if ($response->getStatusCode() != 200) {
        throw new \Exception("Couldn't exclude $zone");
      }
    }

    return;


    ////////////// AFTER SAVE CAMPAIGN STOPS FOR SOME REASON.

    // Get edit page.
    $client = new Client(["cookies" => true]);
    $response = $client->get(
      $this->baseUrl . $this->campaignEditUrl . $campaignId,
      ["cookies" => $this->cookieJar]
    );

    if ($response->getStatusCode() != 200) {
      throw new \Exception("Couldn't get campaign edit");
    }

    // Get html for edit page and assign defaults.
    $html = HtmlDomParser::str_get_html($response->getBody());
    $params = $this->getCampaignEditDefaults($html);

   /* $params['data']['Campaign']['inclusions'] = NULL;
    $params['data']['Campaign']['website_targeting_id'] = NULL;
    $params['data']['Campaign']['website_targeting_name'] = NULL;
    $params['data']['Campaign']['website_targeting_mode'] = "include";
    $params['data']['Campaign']['ct_wst_change'] = NULL;
    $params['data']['Campaign']['website_targeting_items'] = NULL;*/

    // Set up defaults.
    $params['data']['Campaign']['website_targeting'] = 'exclude';
    $params['data']['Campaign']['exclusions'] = implode(PHP_EOL, $zonesList);
    $params['data']['Campaign']['adtype_other'] = 1;
    $params['data']['Campaign']['revshare'] = "auto_ovb";
    $params['data']['Campaign']["url"] = str_replace("&amp;", "&", $params['data']['Campaign']["url"]);

    // Get post url.
    $urlEditPost = $html->find("form", 0)->getAttribute("action");
    $html->clear();

    // Perform edit campaign post request.
    $response = $client->post(
      $this->baseUrl . $urlEditPost,
      [
        "cookies" => $this->cookieJar,
        "form_params" => $params
      ]
    );

    // Check on request success.
    if ($response->getStatusCode() != 200) {
      throw new \Exception("Couldn't update campaign");
    }

    // Check on success by searching for text on the page.
    $html = HtmlDomParser::str_get_html($response->getBody());
    $success = $html->find("span.good", 0)
      && $html->find("span.good", 0)->innertext() == "Your campaign has been updated.";
    $html->clear();


    if (!$success) {
      throw new \Exception("Couldn't find element to confirm campaign update");
    }

  }

  /**
   * Parse through the edit campaign page to collect default data.
   *
   * @param \simplehtmldom_1_5\simple_html_dom $html
   *   Html tree object.
   *
   * @return array
   *   Array of predefined values.
   */
  protected function getCampaignEditDefaults($html) {

    // Assume time won't be changed during automatic testing.
    for ($d = 0; $d < 7; $d++) {
      for ($h = 0; $h < 24; $h++) {
        // TIME IS 0 - day, 01,02 - HOUR so MONDAY 1 hour am 101 sunday is 0
        $time[] = (string) $d . ((string) sprintf("%02d", $h));
      }
    }

    // Get default fields to handle.
    $defaults = $this->getDefaultFields();
    $params['data']['Campaign']['time_targeting'] = implode(",", $time);
    $params['_method'] = $html->find('input[name="_method"]', 0)->getAttribute("value");

    // Get inputs.
    foreach ($defaults['inputs'] as $input) {
      $splitted = preg_split('/[\[]/', $input);
      $params[$splitted[0]][rtrim($splitted[1],"]")][rtrim($splitted[2],"]")] =
        $html->find("input[name=$input]", 0)->getAttribute("value");
    }

    // Get textareas.
    foreach ($defaults['textarea'] as $textarea) {
      $splitted = preg_split('/[\[]/', $textarea);
      $params[$splitted[0]][rtrim($splitted[1],"]")][rtrim($splitted[2],"]")] =
        $html->find("textarea[name=$textarea]", 0)->innertext();
    }

    // Get selects.
    foreach ($defaults['select'] as $select) {
      $splitted = preg_split('/[\[]/', $select);
      if (isset($splitted[3])) {
        foreach ($html->find("select[name=$select]", 0)->find("option") as $option) {
          $params[$splitted[0]][rtrim($splitted[1], "]")][rtrim($splitted[2], "]")][] =
            $option->getAttribute("value");
        }
      } else {
        $params[$splitted[0]][rtrim($splitted[1], "]")][rtrim($splitted[2], "]")] =
          $html->find("select[name=$select] option[selected=selected]", 0)->getAttribute("value");
      }
    }

    return $params;
  }

  /**
   * Login to traffic souce site and save session for next time.
   *
   * @param string $login
   *   Login on popads.
   * @param $password
   *   Password on popads.
   */
  public function login($login, $password) {

    // First check if saved session is still valid.
    if ($this->checkIfLoggedIn($login)) {
      return;
    }

    // Get did challenge tokens.
    $client = new Client(['cookies' => true]);
    $response = $client->get($this->baseUrl . $this->didChallengeUrl);

    // Check on request success and retrieve data.
    if ($response->getStatusCode() == 200) {
      $data = explode("|", $response->getBody()->getContents());
    } else {
      throw new \Exception("Couldn't get did source data.");
    }

    // Solve did challenge with prepared script. Node js is required as global dependency.
    $did = trim(shell_exec(
      "node " . $this->binPath . "/did.js " . $data[0] . " " . $data[1]
    ));

    // Check if did solution is a number.
    if (!is_numeric($did)) {
      throw new \Exception("Couldn't get did solution.");
    }

    // Login data. And Post request.
    $postData = [];
    $postData["User"] = [
      "username" => $login,
      "password" => $password,
      "remember_me" => 1
    ];

    $response = $client->post(
      $this->baseUrl . $this->loginUrl . "?did=" . $did,
      [
        "headers" => ["Content-Type" => "application/x-www-form-urlencoded"],
        "form_params" => ["data" => $postData]
      ]
    );

    // Check on request success.
    if ($response->getStatusCode() != 200) {
      throw new \Exception("Couldn't login.");
    }

    // Save cookies in the bin folder.
    $this->cookieJar = $client->getConfig('cookies');
    file_put_contents( $this->sessionsPath . "/" . $login, serialize($this->cookieJar));

    // Mark as logged in.
    $this->loggedIn = true;
  }


  /**
   * Checks on if user session is still valid.
   * @param $login
   *
   * @return bool
   * @throws \Exception
   */
  protected function checkIfLoggedIn($login) {
    // Check if session cookies exist.
    if (!file_exists($this->sessionsPath . "/" . $login)) {
      return false;
    }

    // Load cookies as cookie jar.
    $this->cookieJar = unserialize(file_get_contents($this->sessionsPath . "/" . $login));

    // Perform requests to dashboard page to see if it is accessible.
    $client = new Client(["cookies" => true]);
    $response = $client->get($this->baseUrl . $this->dashboardUrl, ["cookies" => $this->cookieJar]);
    if ($response->getStatusCode() != 200) {
      throw new \Exception("Couldn't perform test login request to dashboard.");
    }

    // Search for correct dashboard title to define if user session is ok.
    $body = $response->getBody()->getContents();
    $html = HtmlDomParser::str_get_html($body);
    $dashboardTitle = $html->find('title', 0)->innertext();
    $html->clear();

    // Check the title.
    if ($dashboardTitle == "PopAds - Dashboard") {
      $this->loggedIn = true;
      return true;
    } else {
      return false;
    }

  }

  /**
   * Get default fields for campaign change request.
   *
   * @return array
   */
  protected function getDefaultFields() {
    $data['inputs'] = [
      "data[Campaign][name]",
      "data[Campaign][url]",
      "data[Campaign][prefetching_single]",
      "data[Campaign][prefetching_url]",
      'data[Campaign][ovb_max_bid]',
      "data[Campaign][max_per_day]",
      "data[Campaign][url_mode]",
      "data[Campaign][throttling]",
      "data[Campaign][os]",
      "data[Campaign][browser]",
      "data[Campaign][form_factor]",
      "data[Campaign][device]",
      "data[Campaign][ct_mode]",
      "data[Campaign][targeted_isp_list]",
      "data[Campaign][category_id]",
      "data[Campaign][targeted_region_list]"
    ];

    $data['textarea'] = [
      "data[Campaign][url_multi]",
      "data[Campaign][ip_targeting_items]",
      "data[Campaign][inclusions]",
    ];

    $data['select'] = [
      "data[Campaign][unselected_country_codes][]",
      "data[Campaign][country_codes][]",
      "data[Campaign][minimum_quality]",
      "data[Campaign][frequency_cap]",
      "data[Campaign][primespot]",
      "data[Campaign][ref_blank]",
      "data[Campaign][adblock]",
      //"data[Campaign][region_list][]",
      //"data[Campaign][unselected_region_list][]",
      "data[Campaign][unselected_targeted_language_list][]",
      "data[Campaign][unselected_targeted_language_list][]",
      "data[Campaign][targeted_language_list][]",
      "data[Campaign][unselected_targeted_population_list][]",
      "data[Campaign][targeted_population_list][]",
      "data[Campaign][unselected_targeted_resolution_list][]",
      "data[Campaign][targeted_resolution_list][]",
      "data[Campaign][unselected_targeted_conntype_list][]",
      "data[Campaign][targeted_conntype_list][]",
      "data[Campaign][unselected_targeted_connspeed_list][]",
      "data[Campaign][targeted_connspeed_list][]",
      "data[Campaign][isp_list][]",
      "data[Campaign][unselected_isp_list][]",
      "data[Campaign][adscore_ok]",
      "data[Campaign][adscore_proxy]",
      "data[Campaign][adscore_junk]",
      "data[Campaign][adscore_bot]",
      "data[Campaign][timezone]",
    ];

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeReport(array $report) {
    $all = [];
    foreach ($report as $row) {
      $all[] = [
        'impressions' => $row['impressions'],
        'cost' => $row['cost'],
        'id' => $row['website_id'],
        'conversions' => $row['conversion_count'],
        'payout' => $row['conversion_value']
      ];
    }

    return $all;
  }

}
