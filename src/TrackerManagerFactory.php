<?php

namespace AffMarketingScripts;

use AffMarketingScripts\TrackerManagers\KeitaroManager;

/**
 * Class TrackerManagerFactory
 * Gets tracker class.
 *
 * @package AffMarketingScripts
 */
class TrackerManagerFactory {

  /**
   * @const Tracker types.
   */
  const KEITARO = 'keitaro';

  /**
   * Return traffic manager object.
   *
   * @param array $trackerManagerData
   *   Plugrush Data.
   *
   * @return \AffMarketingScripts\TrackerManagers\TrackerManagerInterface
   *   Class object which implements tracker manager interface.
   *
   * @throws \Exception
   *   On maanger class not found.
   */
  public static function getTrackerManager(array $trackerManagerData) {
    switch ($trackerManagerData['name']) {
      case self::KEITARO:
        return new KeitaroManager(
          $trackerManagerData['database']['name'],
          $trackerManagerData['database']['server'],
          $trackerManagerData['database']['port'],
          $trackerManagerData['database']['user'],
          $trackerManagerData['database']['password']
        );
      default:
        throw new \Exception("Couldn't init tracker manager class - config is invalid.");
    }
  }


}