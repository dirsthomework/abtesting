<?php
define("CALC_THRESHOLD", 100);

require_once __DIR__ . "/../vendor/autoload.php";

use AffMarketingScripts\TrackerManagers\KeitaroManager;
use AffMarketingScripts\TrafficSourcePlugins\PlugrushPlugin;

//$csv_pop = array_map('str_getcsv', file('popads_impressions.csv'));
//$csv_ktr = array_map('str_getcsv', file('ktr_impressions.csv'));
$mngr = new KeitaroManager("keitaro", '*.*.*.*', 3306, "root","*");
$pl = new PlugrushPlugin(
  "*@gmail.com",
  '*',
  396487
);
$trackerData = $mngr->getWebsitesReport(22);
$sourceData  = $pl->getReportByZones(7809784);
$sourceData  = $pl->normalizeReport($sourceData);

$a = [];

$avg = 0;
$cnt = 0;
foreach ($sourceData as $one_pop) {
  foreach ($trackerData as $one_ktr) {
    if ($one_pop['id'] == $one_ktr['id'] ) {
      $prc = (100 - 100 * $one_ktr['impressions'] / $one_pop['impressions']);
      $avg += $one_ktr['impressions'];
      $cnt += $one_pop['impressions'];
      
      $a[] = [
        "id" => $one_pop['id'],
        "%"  => $prc,
        "impr" => $one_pop['impressions'],
        "KTR VS ADS" =>  $one_ktr['impressions'] . "/" . $one_pop['impressions'],
        'CV' => $one_ktr['sale'] + $one_ktr['lead']
      ]; 
      break;
    }
  }
}

usort(
  $a,
  function ($x, $y) {
    if ($x["%"] == $y["%"]) return 0;
    return ($x["%"] > $y["%"]) ? -1 : 1; 
  }
);


foreach ($a as $o) {
  if ($o['%'] > 15 and $o['CV'] >= 0) {
    print $o['id'] . " = " . $o['%'] . "-> " . $o['KTR VS ADS'] . "-> CVR = " . $o["CV"] . PHP_EOL;
  }
}

print "AVG LOST " . (100 - 100 * $avg / $cnt) . PHP_EOL;
