<?php

require_once __DIR__ . "/vendor/autoload.php";

use AffMarketingScripts\AbBayesianBandit;


echo "How many conversions? ";
$conversions = rtrim(fgets(STDIN));

echo "How many clicks/impressions? ";
$total = rtrim(fgets(STDIN));

echo "What is the payout ($)? ";
$payout = rtrim(fgets(STDIN));

echo "What is your click cost ($)? ";
$clickCost = rtrim(fgets(STDIN));

echo "What minimum ROI (%) you wanna stick with? ";
$minROI = rtrim(fgets(STDIN));

$minCTR = ($minROI/100+1) * $clickCost / $payout * 100;
print $minCTR;

$bandit = new AbBayesianBandit([], 1);
$bandit->getModel()->alpha += $conversions;
$bandit->getModel()->beta  += $total - $conversions;

echo "==============================================\n";
print "Lower possible CTR: " . 100*$bandit->getConfidenceIntervalLower() . "%\n";
print "Upper possible CTR: " . 100*$bandit->getConfidenceIntervalUpper() . "%\n";
echo "==============================================\n";

if ($minCTR >= 100*$bandit->getConfidenceIntervalUpper()) {
  echo "It is not worth it";
} elseif ($minCTR <= 100*$bandit->getConfidenceIntervalLower()) {
  echo "This is a winner. Somebody is gonna make big bucks"; 
} else {
  echo "Min CTR $minCTR% is in the Confidence range. It's worth to be tested a bit more";
}
echo "\n";

//print_r($interval->getInterval()); // Array([0] => 0.67282656258608, [1] => 0.81400207671582)
//echo $interval->getScore();
